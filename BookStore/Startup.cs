﻿using System.Reflection;
using AutoMapper;
using BookStore.BL;
using BookStore.BL.Services;
using BookStore.DAL;
using BookStore.DAL.Impl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //create default identity model 
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            var connectionString = Configuration.GetConnectionString("BookStore");

            services.AddDbContext<IdentityDbContext>(opt => opt.UseSqlServer(connectionString,
                                                            sql => sql.MigrationsAssembly(migrationAssembly)));

            services.AddIdentityCore<IdentityUser>(options => { }).AddRoles<IdentityRole>()
                                                                  .AddEntityFrameworkStores<IdentityDbContext>()
                                                                  .AddSignInManager();
            services.AddAutoMapper();

            // Dependency Injection
            services.AddSingleton<IUnitOfWork, UnitOfWork>();  //create one instance per application (Singleton)
            services.AddScoped<DbContext, ApplicationContext>();

            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IProductService, ProductService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseAuthentication();
        }
    }
}
