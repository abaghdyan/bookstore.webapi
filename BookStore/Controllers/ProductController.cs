﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookStore.BL;
using BookStore.BL.DTOs;
using BookStore.VMs;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        // GET api/Product
        [HttpGet]
        public IEnumerable<ProductVM> Get()
        {
            var productsDTO = _productService.GetAll();
            foreach (var productDTO in productsDTO)
            {

                ProductVM productVM = _mapper.Map<ProductVM>(productDTO);
                yield return productVM;
            }
        }

        // GET api/Product/5
        [HttpGet("{id}")]
        public ProductVM Get(int id)
        {
            ProductDTO productDTO = _productService.GetProductById(id);
            ProductVM responce = _mapper.Map<ProductVM>(productDTO);
            return responce;
        }

        // POST api/Product
        [HttpPost]
        public void Post([FromBody] ProductVM product)
        {
            ProductDTO productDTO = _mapper.Map<ProductDTO>(product);
            _productService.Create(productDTO);
        }

        // PUT api/Product/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ProductVM product)
        {
            ProductDTO productDTO = _mapper.Map<ProductDTO>(product);
            productDTO.Id = id;   // TODO FOR WHAT???
            _productService.Update(productDTO);
        }

        // DELETE api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id, [FromBody] ProductVM product)
        {
            ProductDTO productDTO = _mapper.Map<ProductDTO>(product);
            productDTO.Id = id;
            _productService.Delete(productDTO);
        }
    }
}
