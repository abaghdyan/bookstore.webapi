﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookStore.BL;
using BookStore.BL.DTOs;
using BookStore.VMs;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public ClientController(IClientService clientService, IMapper mapper)
        {
            _clientService = clientService;
            _mapper = mapper;
        }

        // GET api/Client
        [HttpGet]
        public IEnumerable<ClientVM> Get()
        {
            var clientsDTO = _clientService.GetAll();
            foreach (var clientDTO in clientsDTO)
            {
                ClientVM clientVM = _mapper.Map<ClientVM>(clientDTO);
                yield return clientVM;
            }
        }

        // GET api/Client/5
        [HttpGet("{id}")]
        public ClientVM Get(int id)
        {
            ClientDTO clientDTO = _clientService.GetClientById(id);
            ClientVM responce = _mapper.Map<ClientVM>(clientDTO);
            return responce;
        }

        // POST api/Client
        [HttpPost]
        public void Post([FromBody] ClientVM client)
        {
            ClientDTO clientDTO = _mapper.Map<ClientDTO>(client);
            _clientService.Create(clientDTO);
        }

        // PUT api/Client/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] ClientVM client)
        {
            ClientDTO clientDTO = _mapper.Map<ClientDTO>(client);
            clientDTO.Id = id;   // TODO FOR WHAT???
            _clientService.Update(clientDTO);
        }

        // DELETE api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id, [FromBody] ClientVM client)
        {
            ClientDTO clientDTO = _mapper.Map<ClientDTO>(client);
            clientDTO.Id = id;
            _clientService.Delete(clientDTO);
        }
    }
}
