﻿
using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.VMs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.ProfilesPL
{
    class ClientProfilePL : Profile
    {
        public ClientProfilePL()
        {
            CreateMap<ClientDTO, ClientVM>();
            CreateMap<ClientVM, ClientDTO>();
        }
    }
}
