﻿
using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.VMs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.ProfilesPL
{
    class ProductProfilePL : Profile
    {
        public ProductProfilePL()
        {
            CreateMap<ProductDTO, ProductVM>();
            CreateMap<ProductVM, ProductDTO>();
        }
    }
}
