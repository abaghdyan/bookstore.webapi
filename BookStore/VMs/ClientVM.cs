﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.VMs
{
    public class ClientVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public byte Age { get; set; }
        public string Email { get; set; }
    }
}
