﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.VMs
{
    public class ProductVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public DateTime PublicationDate { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
    }
}
