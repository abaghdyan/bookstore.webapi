﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DAL
{
    public interface IActionRepository<TEntity, TPrimaryKey>
    {
        void Create(TEntity entity);
        void CreateAsync(TEntity entity);
        void CreateMany(IEnumerable<TEntity> entities);
        void CreateManyAsync(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        void Delete(TEntity entity);
        void DeleteById(TPrimaryKey id);
        void DeleteByIdAsync(TPrimaryKey id);
        void DeleteRange(IEnumerable<TEntity> entities);
    }
}
