﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository Products { get; }
        IClientRepository Clients { get; }

        void Commit();
        void RejectChanges();
    }
}
