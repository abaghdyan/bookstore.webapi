﻿using BookStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DAL
{
    public interface IClientRepository : IBaseRepository<Client, int>
    {
    }
}
