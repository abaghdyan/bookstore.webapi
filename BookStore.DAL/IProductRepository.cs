﻿using BookStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DAL
{
    public interface IProductRepository : IBaseRepository<Product, int>
    {
    }
}
