﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DAL
{
    public interface IReadOnlyRepository<TEntity, TPrimaryKey>
    {
        bool Any(Expression<Func<TEntity, bool>> predicate);
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate);
        TEntity Find(TPrimaryKey pkey);
        Task<TEntity> FindAsync(TPrimaryKey pkey);
    }
}
