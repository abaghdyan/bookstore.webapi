﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DAL
{
    public interface IBaseRepository<TEntity, TPrimaryKey> : IReadOnlyRepository<TEntity, TPrimaryKey>, IActionRepository<TEntity, TPrimaryKey>, IRepository
        where TEntity : class, new()
    {
    }
}
