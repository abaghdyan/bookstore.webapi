﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DAL.Impl
{
    public class BaseRepository<TEntity, TPrimaryKey> : IBaseRepository<TEntity, TPrimaryKey>
        where TEntity : class, new()
    {
        protected readonly DbContext _context;
        protected BaseRepository(DbContext dbContext)
        {
            _context = dbContext;
        }

        public void Create(TEntity entity)
                => _context.Set<TEntity>().Add(entity);

        public void CreateAsync(TEntity entity)
                => _context.Set<TEntity>().AddAsync(entity);

        public void CreateMany(IEnumerable<TEntity> entities)
                => _context.Set<TEntity>().AddRange(entities);

        public void CreateManyAsync(IEnumerable<TEntity> entities)
                => _context.Set<TEntity>().AddRangeAsync(entities);

        public void DeleteById(TPrimaryKey pkey)
        {
            var entityToRemove = Find(pkey);
            _context.Set<TEntity>().Remove(entityToRemove);
        }
        public async void DeleteByIdAsync(TPrimaryKey pkey)
        {
            var entityToRemove = await FindAsync(pkey);
            Delete(entityToRemove);
        }

        public void Delete(TEntity entity)
                => _context.Set<TEntity>().Remove(entity);

        public void DeleteRange(IEnumerable<TEntity> entities)
                => _context.Set<TEntity>().RemoveRange(entities);

        public bool Any(Expression<Func<TEntity, bool>> predicate)
                => _context.Set<TEntity>().Any(predicate);

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
                => await _context.Set<TEntity>().AnyAsync(predicate);

        public TEntity Find(TPrimaryKey pkey)
                => _context.Set<TEntity>().Find(pkey);

        public async Task<TEntity> FindAsync(TPrimaryKey pkey)
                => await _context.Set<TEntity>().FindAsync(pkey);

        public IEnumerable<TEntity> GetAll()
                => _context.Set<TEntity>();

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> predicate)
                => _context.Set<TEntity>().Where(predicate);

        public void Update(TEntity entity)
                => _context.Set<TEntity>().Update(entity);

        public void UpdateRange(IEnumerable<TEntity> entities)
                => _context.Set<TEntity>().UpdateRange(entities);
    }
}
