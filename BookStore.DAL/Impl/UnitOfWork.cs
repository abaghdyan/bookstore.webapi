﻿using BookStore.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.DAL.Impl
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _dbContext = new ApplicationContext();
        private Dictionary<string, object> _repositories;
        private bool disposed = false;
        
        public UnitOfWork()
        {
            _repositories = new Dictionary<string, object>();
        }

        public IProductRepository Products => Repository<ProductRepository>();
        public IClientRepository Clients => Repository<ClientRepository>();

        private TRepository Repository<TRepository>()
            where TRepository : IRepository
        {
            var type = typeof(TRepository);
            if (!_repositories.ContainsKey(type.Name))
            {
                var obj = Activator.CreateInstance(type, _dbContext);
                _repositories.Add(type.Name, obj);
            }
            return (TRepository)_repositories[type.Name];
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void RejectChanges()
        {
            var unchanged = _dbContext.ChangeTracker.Entries()
            .Where(e => e.State != EntityState.Unchanged);
            foreach (var entry in unchanged)
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }


        #region Dispose Region
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
