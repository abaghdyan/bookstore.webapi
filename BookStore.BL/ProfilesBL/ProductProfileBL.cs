﻿
using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BL.ProfilesBL
{
    class ProductProfileBL : Profile
    {
        public ProductProfileBL()
        {
            CreateMap<Product, ProductDTO>();
            CreateMap<ProductDTO, Product>();
        }
    }
}
