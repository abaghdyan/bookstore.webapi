﻿
using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BL.ProfilesBL
{
    class ClientProfileBL : Profile
    {
        public ClientProfileBL()
        {
            CreateMap<Client, ClientDTO>();
            CreateMap<ClientDTO, Client>();
        }
    }
}
