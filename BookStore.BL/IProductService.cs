﻿using System;
using System.Collections.Generic;
using System.Text;
using BookStore.BL.DTOs;

namespace BookStore.BL
{
    public interface IProductService
    {
        ProductDTO GetProductById(int id);
        IEnumerable<ProductDTO> GetAll();
        void Create(ProductDTO product);
        void Update(ProductDTO product);
        void Delete(ProductDTO product);
    }
}
