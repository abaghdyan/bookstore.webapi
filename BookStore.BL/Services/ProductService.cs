﻿using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.DAL;
using BookStore.DAL.Models;
using System;
using System.Collections.Generic;

namespace BookStore.BL.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public ProductDTO GetProductById(int id)
        {
            Product _product = _unitOfWork.Products.Find(id);
            ProductDTO productDTO = _mapper.Map<ProductDTO>(_product);
            return productDTO;
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            var products = _unitOfWork.Products.GetAll();
            foreach (var product in products)
            {
                ProductDTO productDTO = _mapper.Map<ProductDTO>(product);
                yield return productDTO;
            }
        }

        public void Create(ProductDTO product)
        {
            Product _product = _mapper.Map<Product>(product);
            _unitOfWork.Products.Create(_product);
            _unitOfWork.Commit();
        }

        public void Update(ProductDTO product)
        {
            Product _product = _mapper.Map<Product>(product);
            _unitOfWork.Products.Update(_product);
            _unitOfWork.Commit();
        }

        public void Delete(ProductDTO product)
        {
            Product _product = _mapper.Map<Product>(product);
            _unitOfWork.Products.Delete(_product);
            _unitOfWork.Commit();
        }

    }
}
