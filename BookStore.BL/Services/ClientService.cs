﻿using AutoMapper;
using BookStore.BL.DTOs;
using BookStore.DAL;
using BookStore.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BL.Services
{
    public class ClientService : IClientService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        public ClientService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public ClientDTO GetClientById(int id)
        {
            var client = _unitOfWork.Clients.Find(id);
            ClientDTO _client = _mapper.Map<ClientDTO>(client);
            return _client;
        }

        public IEnumerable<ClientDTO> GetAll()
        {
            var clients = _unitOfWork.Clients.GetAll();
            foreach (var client in clients)
            {
                ClientDTO clientDTO = _mapper.Map<ClientDTO>(client);
                yield return clientDTO;
            }
        }

        public void Create(ClientDTO client)
        {
            Client _client = _mapper.Map<Client>(client);
            _unitOfWork.Clients.Create(_client);
            _unitOfWork.Commit();
        }

        public void Update(ClientDTO client)
        {
            Client _client = _mapper.Map<Client>(client);
            _unitOfWork.Clients.Update(_client);
            _unitOfWork.Commit();
        }

        public void Delete(ClientDTO client)
        {
            Client _client = _mapper.Map<Client>(client);
            _unitOfWork.Clients.Delete(_client);
            _unitOfWork.Commit();
        }
    }
}
