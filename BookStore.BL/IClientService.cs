﻿using BookStore.BL.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BL
{
    public interface IClientService
    {
        ClientDTO GetClientById(int id);
        IEnumerable<ClientDTO> GetAll();
        void Create(ClientDTO client);
        void Update(ClientDTO client);
        void Delete(ClientDTO client);
    }
}
